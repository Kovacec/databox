<?php declare(strict_types=1);

use App\Databox\Payload;
use App\Databox\Client;
use Carbon\Carbon;

/**
 * Test that Databox Client Wrapper is working es expected.
 * We do not MOCK it since we want to be sure, Databox is available for pushing.
 *
 * Class DataboxClientTest
 */
class DataboxClientTest extends TestCase
{
    public function test_push(): void
    {
        $client = new Client();

        $response = $client->push('test_push', new Payload('$test_push', mt_rand(1,5)/3, new Carbon(), [
            'attribute1' => 'attribute_val_1',
            'attribute2' => 'attribute_val_2',
        ], "EUR"));

        $this->assertTrue($response->ok());
    }

    public function test_push_all(): void
    {
        $client = new Client();

        $response = $client->pushAll('test_push_all', [
            new Payload('test_push_1', mt_rand(1,5)/3),
            new Payload('test_push_1', mt_rand(1,5)/3, new Carbon()),
            new Payload('test_push_2', mt_rand(1,5)/3, new Carbon(), [
                'attribute1' => 'attribute_val_1',
                'attribute2' => 'attribute_val_2',
            ], "EUR")
        ]);

        $this->assertTrue($response->ok());
    }
}
