<?php declare(strict_types=1);

use App\Databox\Payload;
use App\Metrics\COVID19;
use App\Services\COVID19Service;
use App\Databox\Client as DataboxClient;
use App\DataProviders\Client as DataProviderClient;
use App\DataProviders\Response as DataProviderResponse;
use Carbon\Carbon;

class COVID19PushServiceTest extends TestCase
{
    /**
     * Note this will double the data each time we make a test.
     *
     * @test
     * @return void
     */
    public function fetch_live_by_country_all_status(): void
    {
        $databoxClientMock = $this->getMockBuilder(DataboxClient::class)->disableOriginalConstructor()->getMock();
        $dataProviderClientMock = $this->getMockBuilder(DataProviderClient::class)->disableOriginalConstructor()->getMock();

        $response = new DataProviderResponse();
        $response->setStatusCode(200);
        $response->setBody('[
          {
            "Country": "Switzerland",
            "CountryCode": "CH",
            "Lat": "46.82",
            "Lon": "8.23",
            "Confirmed": 20505,
            "Deaths": 666,
            "Recovered": 6415,
            "Active": 13424,
            "Date": "2020-04-04T00:00:00Z",
            "LocationID": "628d4f12-6ebe-4fa9-b046-77ff0b894a4e"
          },
          {
            "Country": "Switzerland",
            "CountryCode": "CH",
            "Lat": "46.82",
            "Lon": "8.23",
            "Confirmed": 20505,
            "Deaths": 666,
            "Recovered": 6415,
            "Active": 13424,
            "Date": "2020-04-05T00:00:00Z",
            "LocationID": "628d4f12-6ebe-4fa9-b046-77ff0b894a4e"
          }
        ]');

        $dataProviderClientMock->method("get")->willReturn($response);
        $system_under_test = new COVID19Service($databoxClientMock, $dataProviderClientMock);

        $result = $system_under_test->fetchLiveByCountryAllStatus('switzerland');
        $this->assertTrue($result->ok());

        $payload = $result->getPayload();
        $this->assertCount(2 * count(COVID19::getConstList()), $payload); // 2 elements for each metric
        $this->assertInstanceOf(Payload::class, $payload[0]);
    }


    public function test_response_will_have_an_error_set(): void
    {
        $databoxClientMock = $this->getMockBuilder(DataboxClient::class)->disableOriginalConstructor()->getMock();
        $dataProviderClientMock = $this->getMockBuilder(DataProviderClient::class)->disableOriginalConstructor()->getMock();

        $response = new DataProviderResponse();
        $response->setStatusCode(503);
        $response->setBody('Service Unavailable');
        $dataProviderClientMock->method("get")->willReturn($response);
        $system_under_test = new COVID19Service($databoxClientMock, $dataProviderClientMock);
        $result = $system_under_test->fetchLiveByCountryAllStatus('slovenia');
        $this->assertFalse($result->ok());

        $response->setStatusCode(400);
        $dataProviderClientMock->method("get")->willReturn($response);
        $result = $system_under_test->fetchLiveByCountryAllStatus('slovenia');
        $this->assertFalse($result->ok());
    }

    public function test_response_will_have_error_when_data_provider_response_body_is_empty(): void
    {
        $response = new DataProviderResponse();
        $response->setStatusCode(200);
        $response->setBody('');

        $this->assertEquals('ResponseEmpty', $response->getError()->getType());
    }


    public function test_fetch_live_by_country_and_status_after_date(): void
    {
        $databoxClientMock = $this->getMockBuilder(DataboxClient::class)->disableOriginalConstructor()->getMock();
        $dataProviderClientMock = $this->getMockBuilder(DataProviderClient::class)->disableOriginalConstructor()->getMock();

        $response = new DataProviderResponse();
        $response->setStatusCode(200);
        $response->setBody('[
          {
            "Country": "Switzerland",
            "CountryCode": "CH",
            "Lat": "46.82",
            "Lon": "8.23",
            "Confirmed": 20505,
            "Deaths": 666,
            "Recovered": 6415,
            "Active": 13424,
            "Date": "2020-04-04T00:00:00Z",
            "LocationID": "628d4f12-6ebe-4fa9-b046-77ff0b894a4e"
          }
        ]');

        $dataProviderClientMock->method("get")->willReturn($response);
        $system_under_test = new COVID19Service($databoxClientMock, $dataProviderClientMock);

        $result = $system_under_test->fetchLiveByCountryAndStatusAfterDate('switzerland', new Carbon());
        $this->assertTrue($result->ok());

        $payload = $result->getPayload();
        $this->assertCount(count(COVID19::getConstList()), $payload); // 2 elements for each metric
        $this->assertInstanceOf(Payload::class, $payload[0]);
    }

}
