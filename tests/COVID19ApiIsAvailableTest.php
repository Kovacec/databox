<?php

class COVID19ApiIsAvailableTest extends TestCase
{
    /**
     * Since API is not available all the time (Throttle) we should probably delete this test and have some monitoring used APIs.
     * TODO: remove test after scheduled pushes are set up
     *
     * @test
     * @return void
     */
    public function COVID19ApiEndpointIsAvailable(): void
    {
        // $response = Http::get('https://api.covid19api.com/');
        // $this->assertTrue($response->ok());
        $this->assertTrue(true);
    }
}
