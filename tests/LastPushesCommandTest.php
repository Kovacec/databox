<?php


class LastPushesCommandTest extends TestCase
{
    public function test_importing_without_argument()
    {
        $this->expectException(Symfony\Component\Console\Exception\RuntimeException::class);
        $this->artisan('databox:last');
    }


    public function test_will_return()
    {
        $this->artisan('databox:last 5');
        $this->assertTrue(true); // no exception
    }
}
