<?php

class PushCommandTest extends TestCase
{
    public function test_importing_without_argument()
    {
        $this->expectException(Symfony\Component\Console\Exception\RuntimeException::class);
        $this->artisan('databox:push');
    }

    public function test_push()
    {
        $response = $this->artisan('databox:push test_push 1');
        $this->assertEquals(0, $response, "Can not push test data to Databox.");
    }

}
