<?php


class GithubProjectContributorsCommandTest extends TestCase
{

    public function test_importing_without_argument()
    {
        $this->expectException(Symfony\Component\Console\Exception\RuntimeException::class);
        $this->artisan('databox:github:contributors');
    }


    public function test_command_with_project_that_does_not_exsist()
    {
        $response = $this->artisan("databox:github:contributors databox this-project-you-do-not-have-yet");
        $this->assertEquals(-1, $response);
    }

}
