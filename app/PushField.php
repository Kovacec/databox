<?php declare(strict_types=1);

namespace App;

class PushField
{
    public const SENT_TIME = 'sent_time';
    public const SERVICE = 'service';
    public const METRIC = 'metric';
    public const COUNT = 'count';
    public const OK = 'ok';
    public const ERROR = 'error';
}

