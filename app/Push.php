<?php // declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Push Model For local database pushes table.
 *
 * @package App
 */
class Push extends Model
{
    public const TABLE = 'pushes';

    public $table = self::TABLE;

    public $timestamps = false;

    protected $fillable = [PushField::SERVICE, PushField::METRIC, PushField::COUNT, PushField::OK, PushField::ERROR];

    protected $dates = [PushField::SENT_TIME];
}
