<?php declare(strict_types=1);

namespace App;

/**
 * It is nice to have same error class for DataProviders and Databox.
 * We generally do not want to throw an Exception but rather we should
 * log all exceptions by data providers and Databox to some 3.party
 * monitoring platform like Sentry and implement monitoring.
 *
 * APIs usually have throttle, or become unavailable.
 * In case of throttle (usually response code 503) we could decrease a number of fetch-requests.
 * When they become unavailable we could implement another DataProvider that could fetch same data
 * from other source.
 *
 * @package App
 */
class Error
{
    /**
     * Error type.
     *
     * @var string
     */
    private string $type;

    /**
     * Error message.
     *
     * @var string
     */
    private string $message;

    /**
     * Error constructor.
     * @param string $type
     * @param string $message
     */
    public function __construct(string $type, string $message)
    {
        $this->type = $type;
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

}
