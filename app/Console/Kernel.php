<?php // declare(strict_types=1); extends ConsoleKernel which is not strict

namespace App\Console;

use App\Console\Commands\GithubProjectContributorsCommand;
use App\Console\Commands\PushAsanaCommand;
use App\Console\Commands\PushAsanaCompletedOnTasksCommand;
use App\Console\Commands\PushCommand;
use App\Console\Commands\PushCOVID19Command;
use App\Console\Commands\LastPushesCommand;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        PushAsanaCommand::class,
        PushAsanaCompletedOnTasksCommand::class,
        GithubProjectContributorsCommand::class,
        PushCOVID19Command::class,
        LastPushesCommand::class,
        PushCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('databox:COVID19 slovenia --new')->hourly();
        $schedule->command('databox:COVID19 austria --new')->hourly();
        $schedule->command('databox:COVID19 croatia --new')->hourly();
        $schedule->command('databox:COVID19 hungary --new')->hourly();
        // ...
    }
}
