<?php

/**
 * Rate limit: [per minute]
 * 150 Free
 * 1500 Premium
 *
 *
 * Getting only completed tasks: https://forum.asana.com/t/getting-completed-tasks/1073/5
 *
 * Search: https://developers.asana.com/docs/search-tasks-in-a-workspace
 * 	?completed boolean	Filter to completed tasks
 *
 *
 * Getting completed tasks:
 * https://app.asana.com/api/1.0/workspaces/7582066485656/tasks/search?completed=true&limit=50&assignee.any=bojan@databox.com
 *
 *
 * ?created_on string(date)
 * ?completed_on string(date)	ISO 8601 date string or null
 *
 * ?completed boolean	Filter to completed tasks
 * ?assignee.any string	Comma-separated list of user identifiers
 *
 *
 */

namespace App\Console\Commands;

use App\Databox\Payload;
use App\Databox\Client as DataboxClient;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class PushAsanaCompletedOnTasksCommand extends Command
{
    protected $service;

    protected $signature = "databox:asana:tasks:completed-on {email} {date}";

    protected $description = "Push asana data to databox.";

    private $lastFetchDateTime = '';
    private $rateLimit = 0;

    private function get($uri) {
        $client = new Client();

        $lastFetchTime = date("YmDHi");

        if ($this->lastFetchDateTime != $lastFetchTime) {
            $this->rateLimit = 0;
        } else {
            $this->rateLimit++;
        }

        if ($this->rateLimit > 1499) {
            // to prevent rate limit we have to wait till end of next minute
            $currentSecond = (int)date('s');
            sleep(61 - $currentSecond);
        }

        $headers = [
            'Authorization' => 'Bearer ' . "1/1200432385524025:2e0b241d64a4bdc5c27529cd6e106987",
            'Accept'        => 'application/json',
        ];

        $response = $client->request('GET', $uri, [
            'headers' => $headers
        ]);

        return json_decode($response->getBody());
    }


    public function handle()
    {
        $client = new DataboxClient();
        $userEmail = $this->argument('email');
        $date = new Carbon($this->argument('date'));
        $limit = 50;
        $nextPage = true;
        $tasks = [];

        $base_uri = 'https://app.asana.com/api/1.0/workspaces/7582066485656/tasks/search?limit=' . $limit . '&completed=true&assignee.any=' . $userEmail . '&completed_on=' . $date->format('Y-m-d');

        $offset = null;

        while($nextPage) {

            $uri = $base_uri;

            if ($offset) {
                $uri = $base_uri . '&offset=' . $offset;
            }

            $response = $this->get($uri);

            $tasks = array_merge($tasks, $response->data);

            $nextPage = isset($response->next_page);

            if ($nextPage) {
                $offset = $response->next_page->offset;
            }

        }

        $payload = new Payload('completed_tasks', (float)count($tasks), $date, [
            'user' => $userEmail
        ]);

        $client->push("PUSH", $payload);
        $this->info($userEmail . ': ' . count($tasks));
    }

}
