<?php

namespace App\Console\Commands;

use App\Databox\Client;
use App\Databox\Payload;
use Illuminate\Console\Command;

class PushCommand extends Command
{

    protected $signature = "databox:push {key} {value} {date?} {attributes?} {unit?}";

    protected $description = "Push some data.";

    public function handle()
    {
        $client = new Client();
        $response = $client->push("PUSH", new Payload($this->argument('key'), $this->argument('value')));
        $this->info($response->ok() ? $response->getBody() : $response->getError()->getMessage());
    }
}
