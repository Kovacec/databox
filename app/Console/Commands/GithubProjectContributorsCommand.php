<?php

namespace App\Console\Commands;

use App\Databox\Client;
use App\Databox\Payload;
use App\Services\GithubService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GithubProjectContributorsCommand extends Command
{
    protected $signature = "databox:github:contributors {owner} {project}";

    protected $description = "Get Contributors for the project owned by owner.";

    public function handle(): int
    {
        $client = new Client();

        $token = DB::table("tokens")->where("api", "=", "github")->first();

        if ($token === null)
        {
            $this->error("Access Token For HitHub is missing.");
            $this->info("Open http://localhost:8082 in the browser and authorize GitHub.");
            return -1;
        }

        $service = new GithubService($token->token);

        $result = $service->listContributors($this->argument('owner'), $this->argument('project'));

        if (count($result) === 2 && isset($result["message"]))
        {
            $this->info($result["message"]);

            return -1;
        }

        $key = 'contributors_' . $this->argument('owner') . '_' .$this->argument('project');
        $payload = new Payload($key, count($result));
        $response = $client->push($key, $payload);

        $this->info($response->ok() ? $response->getBody() : $response->getError()->getMessage());

        return 0;
    }
}
