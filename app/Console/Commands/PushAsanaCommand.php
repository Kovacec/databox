<?php

/**
 * Rate limit: [per minute]
 * 150 Free
 * 1500 Premium
 *
 *
 * Getting only completed tasks: https://forum.asana.com/t/getting-completed-tasks/1073/5
 *
 * Search: https://developers.asana.com/docs/search-tasks-in-a-workspace
 * 	?completed boolean	Filter to completed tasks
 *
 *
 * Getting completed tasks:
 * https://app.asana.com/api/1.0/workspaces/7582066485656/tasks/search?completed=true&limit=50&assignee.any=bojan@databox.com
 *
 *
 * ?created_on string(date)
 * ?completed_on string(date)	ISO 8601 date string or null
 *
 * ?completed boolean	Filter to completed tasks
 * ?assignee.any string	Comma-separated list of user identifiers
 *
 *
 */

namespace App\Console\Commands;

use App\Databox\Payload;
use App\Databox\Client as DataboxClient;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class PushAsanaCommand extends Command
{
    protected $service;

    protected $signature = "databox:asana:tasks {email}";

    protected $description = "Push asana data to databox.";

    private $lastFetchDateTime = '';
    private $rateLimit = 0;


    private function get($uri) {
        $client = new Client();

        $lastFetchTime = date("YmDHi");

        if ($this->lastFetchDateTime != $lastFetchTime) {
            $this->rateLimit = 0;
        } else {
            $this->rateLimit++;
        }

        if ($this->rateLimit > 1499) {
            // to prevent rate limit we have to wait till end of next minute
            $currentSecond = (int)date('s');
            sleep(61 - $currentSecond);
        }

        $headers = [
            'Authorization' => 'Bearer ' . "1/1200432385524025:2e0b241d64a4bdc5c27529cd6e106987",
            'Accept'        => 'application/json',
        ];

        $response = $client->request('GET', $uri, [
            'headers' => $headers
        ]);

        return json_decode($response->getBody());
    }


    public function handle()
    {
        $client = new DataboxClient();
        $userEmail = $this->argument('email');

        $now = new Carbon();
        $yesterday = $now->subDay(1);
        $yesterday->setHour(0);
        $yesterday->setMinute(0);
        $yesterday->setSecond(0);

        $data = [];
        $tasks = [];


        $limit = 50;
        $nextPage = true;

        $base_uri = 'https://app.asana.com/api/1.0/tasks?assignee='. $userEmail .'&workspace=7582066485656&limit=' . $limit;
        $offset = null;

        while($nextPage) {

            $uri = $base_uri;

            if ($offset) {
                $uri = $base_uri . '&offset=' . $offset;
            }

            $response = $this->get($uri);

            $data = array_merge($data, $response->data);

            $nextPage = isset($response->next_page);

            if ($nextPage) {
                $offset = $response->next_page->offset;
            }

        }


        /*
        $base_uri = 'https://app.asana.com/api/1.0/tasks/';
        foreach ($data as $task) {
            $uri = $base_uri . $task->gid;
            $response = $this->get($uri);
            $tasks[] = $response->data;

            $this->info('Task fetched: ' . $task->name);
        }
        */


        /*
        $completed = array_filter($tasks, function ($task) {
            return $task->completed;
        });

        $completed_yesterday = array_filter($tasks, function ($task) use ($yesterday) {
            $completedAt = new Carbon($task->complted_at);
            return $task->completed && ($completedAt->format('Ymd') === $yesterday->format('Ymd'));
        });
        */


        // TOTAL TASKS
        $client->push("PUSH", new Payload('asana_total_tasks', count($data)));
        $this->info('Total tasks: ' . count($data));


        // TOTAL TASKS BY USER
        $client->push("PUSH", new Payload('asana_total_tasks_by_user', count($data), null, [
            'user' => $userEmail
        ]));

        $this->info('Total tasks by user [' .  $userEmail . ']:' . count($data));


//        // TOTAL TASKS BY USER with date
//        $client->push("PUSH", new Payload('asana_total_tasks_by_user_with_history', count($tasks), new Carbon(), [
//            'user' => $userEmail
//        ]));
//
//        // TOTAL COMPLETED TASKS
//        $client->push("PUSH", new Payload('asana_total_completed_tasks', count($completed)));
//
//
//        // TOTAL COMPLETED TASKS BY USER
//        $client->push("PUSH", new Payload('asana_total_completed_tasks_by_user_with_history', count($completed)), null, [
//            'user' => $userEmail
//        ]);
//
//        // TOTAL COMPLETED TASKS BY USER with date
//        $client->push("PUSH", new Payload('asana_total_completed_tasks_by_user_with_history', count($completed_yesterday)), $yesterday, [
//            'user' => $userEmail
//        ]);
    }
}
