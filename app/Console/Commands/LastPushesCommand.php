<?php declare(strict_types=1);

namespace App\Console\Commands;

use App\Databox\Client;
use Illuminate\Console\Command;

/**
 * Verify lat pushed data to Databox using API
 *
 * Class LastPushesCommand
 * @package App\Console\Commands
 */
class LastPushesCommand extends Command
{
    protected $signature = "databox:last {number}";

    protected $description = "Will return last sent data packages.";

    public function handle()
    {
        $client = new Client();
        $client->lastPush($this->argument('number'));
        $this->info('done');
    }
}
