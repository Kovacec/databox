<?php // declare(strict_types=1); not possible since Command is not strict yet

namespace App\Console\Commands;

use App\Push;
use App\PushField;
use App\Services\COVID19Service;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\DataProviders\Response as DataProviderResponse;

class PushCOVID19Command extends Command
{
    protected $service;

    protected $signature = "databox:COVID19 {country} {--new}";

    protected $description = "Push live COVID19 data to databox.";

    public function __construct()
    {
        parent::__construct();
        $this->service = new COVID19Service();
    }

    public function handle()
    {
        $this->option('new') ? $this->fetchNew($this->argument('country')) : $this->fetchAll($this->argument('country'));
    }

    private function fetchAll(string $country): void
    {
        $dataProviderResponse = $this->service->fetchLiveByCountryAllStatus($country);
        $this->process($dataProviderResponse);
    }

    private function fetchNew(string $country): void
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $last = Push::where(PushField::SERVICE, "=", COVID19Service::class)
            ->where(PushField::OK, 1)
            ->orderBy("id", "desc")
            ->limit(1)
            ->first();

        if ($last) {
            /** @var Carbon $fromDate */
            $fromDate = $last->getAttribute(PushField::SENT_TIME);
            $dataProviderResponse = $this->service->fetchLiveByCountryAndStatusAfterDate($country, $fromDate);
            $this->process($dataProviderResponse);
        } else {
            $this->fetchAll($country);
        }
    }

    /**
     * Process DataProvider Response
     *
     * @param DataProviderResponse $response
     */
    private function process(DataProviderResponse $response): void
    {
        $this->info($response->getStatusCode());
        if ($response !== null && $response->ok()) {
            $databoxResponse = $this->service->databoxClient()->pushAll(COVID19Service::class, $response->getPayload());
            $this->info($databoxResponse->ok() ? $databoxResponse->getBody() : $databoxResponse->getError()->getMessage());
        }
    }
}

