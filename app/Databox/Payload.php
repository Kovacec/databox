<?php declare(strict_types=1);

namespace App\Databox;

use Carbon\Carbon;

/**
 * the metric name has a $ prepended
 * the metric value is a number (float is allowed)
 * the date, if specified, conforms to the ISO 8601 standard and is compatible with JavaScript Date.parse()
 *
 * Class Payload
 * @package App\Databox
 */
class Payload
{
    /**
     * Metric name
     *
     * @var string
     */
    private string $key;
    /**
     * Metric value
     *
     * @var float
     */
    private float $value;
    /**
     * Date of measurement.
     *
     * @var Carbon|null
     */
    private ?Carbon $date;
    /**
     * Metric attributes
     *
     * @var array|null
     */
    private ?array $attributes;
    /**
     * Metric unit
     *
     * @var string|null
     */
    private ?string $unit;

    public function __construct(string $key, float $value, Carbon $date = null, array $attributes = null, string $unit = null)
    {
        $this->key = $key;
        $this->value = $value;
        $this->date = $date;
        $this->attributes = $attributes;
        $this->unit = $unit;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): void
    {
        $this->key = $key;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function setValue(float $value): void
    {
        $this->value = $value;
    }

    public function getDate(): ?Carbon
    {
        return $this->date;
    }

    public function setDate(Carbon $date): void
    {
        $this->date = $date;
    }

    public function getAttributes(): ?array
    {
        return $this->attributes;
    }

    public function setAttributes(array $attributes): void
    {
        $this->attributes = $attributes;
    }

    public function getUnit(): ?string
    {
        return $this->unit;
    }

    public function setUnit(string $unit): void
    {
        $this->unit = $unit;
    }

    public function toArray(): array
    {
        return [
            $this->getKey(),
            $this->getValue(),
            ($this->getDate()) ? $this->getDate()->toIso8601String() : null,
            $this->getAttributes(),
            $this->getUnit()
        ];
    }
}
