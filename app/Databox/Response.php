<?php declare(strict_types=1);

namespace App\Databox;

use App\Error;

class Response
{
    /**
     * Http Response StatusCode
     *
     * @var int
     */
    protected int $statusCode;

    /**
     * Response body
     *
     * @var string
     */
    private string $body;

    /**
     * @var Error|null
     */
    private ?Error $error;

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode(int $statusCode): void
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody(string $body): void
    {
        $this->body = $body;
    }

    /**
     * @return Error|null
     */
    public function getError(): ?Error
    {
        return $this->error;
    }

    /**
     * @param Error|null $error
     */
    public function setError(?Error $error): void
    {
        $this->error = $error;
    }

    /**
     * Push was successful.
     *
     * @return bool
     */
    public function ok(): bool
    {
        return ($this->statusCode === 200 && $this->body !== null);
    }
}
