<?php declare(strict_types=1);

namespace App\Databox;

use App\Error;
use App\Push;
use App\PushField;
use Databox\Client as DataboxClient;
use GuzzleHttp\Exception\RequestException;

/**
 * Databox Client wrapper.
 *
 * @package App\Databox
 */
class Client
{
    protected DataboxClient $databoxClient;

    public function __construct()
    {
        $this->databoxClient = new DataboxClient(env("DATABOX_TOKEN"));
    }

    /**
     * Push payload to Databox.
     *
     * @param string $service
     * @param Payload $payload
     * @return Response
     */
    public function push(string $service, Payload $payload): Response
    {
        $response = new Response();

        $push = new Push();
        $push->setAttribute(PushField::SERVICE, $service);
        $push->setAttribute(PushField::METRIC, $payload->getKey());
        $push->setAttribute(PushField::COUNT, 1);

        try {

            $result = $this->databoxClient->push(
                $payload->getKey(),
                $payload->getValue(),
                ($payload->getDate() !== null) ? $payload->getDate()->format('Y-m-d H:i:s') : null,
                ($payload->getAttributes() !== null) ?  $payload->getAttributes() : null);

            $response->setStatusCode(200);
            $response->setBody($result);

            $push->setAttribute(PushField::OK, 1);

        } catch (RequestException $exception) {
            $response = $this->parseResponseError($exception->getMessage(), $response);
            $push->setAttribute(PushField::OK, 0);
            $push->setAttribute(PushField::ERROR, $response->getError()->getMessage());
        }

        $push->save();

        return $response;
    }

    /**
     * Push array of payloads (rows) to Databox.
     *
     * @param string $service
     * @param Payload[] $payloads
     * @return Response
     */
    public function pushAll(string $service, array $payloads): Response
    {
        $response = new Response();

        if (count($payloads) === 0)
        {
            $response->setStatusCode(200);
            $response->setBody('No new Data. Provider response is empty.');
            return $response;
        }

        $data = array_map(function ($payload) {
            return $payload->toArray();
        }, $payloads);

        $metrics = $this->getMetricsFromPayloadArray($data);

        try {
            $result = $this->databoxClient->insertAll($data);
            $response->setStatusCode(200);
            $response->setBody($result);
            $this->log($service, $metrics, $data);
        } catch (RequestException $exception) {
            $response = $this->parseResponseError($exception->getMessage(), $response);
            $this->log($service, $metrics, $data, $exception->getMessage());
        }

        return $response;
    }

    /**
     * From documentation: https://developers.databox.com/api/#responses-and-errors
     * {"status":"ok"}, seems to be replaced with identifier (assume key from key-value store)
     * @see DataboxClient::push (Databox\Client::push)
     *
     * Possible Error Codes:
     *
     * {"status":"Error: No authorization sent”}
     * {"status":"Error: Unauthorized - check your token”}
     * {"status":"Error: Too many requests 10/sec is allowed”}
     * {"status":"Error: Content type should be application/json”}
     * {"status":"Error: JSON format error or JSON empty”}
     * {"status":"Error: JSON should be wrapped up in \'data\' item”}
     *
     *
     *
     * {"status":"Error: \'data\' element should contain array of items not single item only”}
     *
     * Actually Response has an additional line of information,
     * with repose code in it.
     *
     * I think it would be nice if Databox API would return just json,
     * or since we are using PHP SKD, some Response Object would be even nicer.
     *
     * @param string $errorMessage
     * @param Response $response
     * @return Response
     */
    private function parseResponseError(string $errorMessage, Response $response): Response
    {
        $resultLines = explode(PHP_EOL, $errorMessage);

        if (count($resultLines) > 1 && strpos($resultLines[0], "resulted in a ") > 0)
        {
            $response->setStatusCode(intval(substr($resultLines[0], strpos($resultLines[0], "resulted in a ") + 15, 3)));
            $responseError = json_decode($resultLines[1]);

            if ($responseError !== null && $responseError->type)
            {
                $response->setError(new Error($responseError->type, $responseError->message));
            }
            else
            {
                $response->setError(new Error('data', $errorMessage));
            }
        }

        return $response;
    }

    /**
     * Only to be used in console for debugging.
     *
     * Seems like an issue. I get only empty array as response from Databox/Client::lastPush,
     * for my account/set-up.
     *
     * @param int $n number of packages returned
     * @return array
     */
    public function lastPush($n = 5): array
    {
        return $this->databoxClient->lastPush($n);
    }

    /**
     * Store last send data to local database.
     * @param string $service
     * @param array $metrics
     * @param array $data
     * @param string|null $errorMessage
     */
    private function log(string $service, array $metrics, array $data, ?string $errorMessage = null): void
    {
        $counts = array_merge(...array_map(function ($metric) use ($data) {
            return [$metric => $this->countSentMetric($data, $metric)];
        }, $metrics));

        foreach ($metrics as $metric)
        {
            $push = new Push();
            $push->setAttribute(PushField::SERVICE, $service);
            $push->setAttribute(PushField::METRIC, $metric);
            $push->setAttribute(PushField::COUNT, $counts[$metric]);
            $push->setAttribute(PushField::OK, $errorMessage ===  null);
            $push->setAttribute(PushField::ERROR, $errorMessage);
            $push->save();
        }
    }

    /**
     * Count how many times metric was sent to Databox.
     *
     * @param array $data
     * @param string $metric
     * @return int
     */
    private function countSentMetric(array $data, string $metric): int
    {
        return count(array_filter($data, function ($row) use ($metric) {
            return $row[0] == $metric;
        }));
    }

    /**
     * Return metrics contained in payload.
     *
     * @param $data
     * @return string[]
     */
    private function getMetricsFromPayloadArray($data): array
    {
        return array_unique(array_map(function ($row) {
            return $row[0];
        }, $data));
    }

}
