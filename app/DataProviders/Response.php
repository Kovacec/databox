<?php declare(strict_types=1);

namespace App\DataProviders;

use App\Error;
use App\Databox\Payload;

class Response
{
    protected int $statusCode;

    protected string $body;

    protected array $payload;

    /**
     * @var Error|null
     */
    private ?Error $error;

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode(int $statusCode): void
    {
        $this->statusCode = $statusCode;

        if ($statusCode !== 200)
        {
            if ($statusCode >= 300 && $statusCode < 400)
            {
                $this->setError(new Error('Redirect', (string)$statusCode));
            }
            else if ($statusCode >= 400 && $statusCode < 500)
            {
                $this->setError(new Error('BadRequest', (string)$statusCode));
            }
            else if ($statusCode >= 500)
            {
                $this->setError(new Error('ServerError', (string)$statusCode));
            }
        }
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody(string $body): void
    {
        $this->body = $body;

        if ($body === '')
        {
            $this->setError(new Error('ResponseEmpty', 'response body is empty'));
        }
    }

    /**
     * Get Databox Payload.
     *
     * @return Payload[]
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    /**
     * @param Payload[] $payload
     */
    public function setPayload(array $payload): void
    {
        $this->payload = $payload;
    }

    /**
     * Was request successful.
     *
     * @return bool
     */
    public function ok(): bool
    {
        return $this->body && $this->body !== null && $this->statusCode == 200;
    }

    /**
     * @return Error|null
     */
    public function getError(): ?Error
    {
        return $this->error;
    }

    /**
     * @param Error|null $error
     */
    public function setError(?Error $error): void
    {
        $this->error = $error;
    }
}
