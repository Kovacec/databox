<?php


namespace App\DataProviders;


use Illuminate\Support\Facades\Http;

/**
 * Class Client
 * @package App\DataProviders
 */
class Client
{
    public function get($url): Response
    {
        $response = new Response();
        $result = Http::get($url);

        $response->setStatusCode($result->toPsrResponse()->getStatusCode());
        $response->setBody($result->body());

        return $response;
    }
}
