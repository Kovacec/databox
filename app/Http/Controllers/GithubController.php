<?php declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\View\View;

class GithubController extends Controller
{
    private $authorizeURL = 'https://github.com/login/oauth/authorize';
    private $tokenURL = 'https://github.com/login/oauth/access_token';
    private $redirectUrl;

    /**
     * GithubController constructor.
     */
    public function __construct()
    {
        // in real world env would be cached and replace with cache
        $this->redirectUrl = env("APP_URL") . "/github/callback";;
    }


    public function redirect(Request $request): RedirectResponse
    {
        $hash = hash('sha256', microtime(TRUE) . rand() . $_SERVER['REMOTE_ADDR']);
        $request->session()->put('state', $hash);
        $request->session()->flash('access_token');

        $params = [
            'client_id' => env("GITHUB_CLIENT_ID"),
            'redirect_uri' => $this->redirectUrl,
            'scope' => 'user',
            'state' => $hash
        ];

        return redirect($this->authorizeURL . '?' . http_build_query($params));
    }

    public function callback(Request $request): View
    {
        $code = $request->get("code");
        $state = $request->get("state");
        $result = [];

        if ($request->session()->get("state") != $state) {
            return view('index', ["error" => "forbidden"]);
        }

        // exchange the auth code for a token
        $response = Http::post($this->tokenURL, [
            'client_id' => env("GITHUB_CLIENT_ID"),
            'client_secret' => env("GITHUB_CLIENT_SECRET"),
            'redirect_uri' => $this->redirectUrl,
            'state' => $request->session()->get('state'),
            'code' => $code
        ]);

        parse_str($response->body(), $result);

        if (isset($result["error"])) {
            return view('index', ["error" => $result["error_description"]]);
        }

        DB::table('tokens')->upsert([
            [
                "api" => "github",
                "token" => $result["access_token"],
                "type" => $result["token_type"]
            ]
        ], "api");

        return view('index');
    }
}
