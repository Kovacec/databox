<?php declare(strict_types=1);

namespace App\Services;

use App\Databox\Client as DataboxClient;
use App\DataProviders\Client as DataProviderClient;

/**
 * Class Service for fetching and pushing data to Databox.
 * We need dataProvider Client to fetch the data and
 * databoxClient to push the data.
 *
 * @package App\Databox
 */
class Service
{
    /**
     * For pushing data.
     *
     * @var DataboxClient
     */
    protected DataboxClient $databoxClient;
    /**
     * For fetching data.
     *
     * @var DataProviderClient
     */
    protected DataProviderClient $dataProviderClient;

    /**
     * Service constructor.
     * @param DataboxClient|null $databoxClient
     * @param DataProviderClient|null $dataProviderClient
     */
    public function __construct(DataboxClient $databoxClient = null, DataProviderClient $dataProviderClient = null)
    {
        ($databoxClient == null) ? $this->databoxClient = new DataboxClient() : $this->databoxClient = $databoxClient;
        ($dataProviderClient == null) ? $this->dataProviderClient = new DataProviderClient() : $this->dataProviderClient = $dataProviderClient;
    }

    public function databoxClient(): DataboxClient
    {
        return $this->databoxClient;
    }

    public function dataProviderClient(): DataProviderClient
    {
        return $this->dataProviderClient;
    }
}
