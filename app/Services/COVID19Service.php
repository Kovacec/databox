<?php declare(strict_types=1);

namespace App\Services;

use App\Databox\Payload;
use App\DataProviders\Response as DataProviderResponse;
use Carbon\Carbon;
use App\Metrics\COVID19;

class COVID19Service extends Service
{
    private const BASE_URL = "https://api.covid19api.com/";

    /**
     * GET Live By Country All Status
     * https://api.covid19api.com/live/country/south-africa
     *
     * @param string $country
     * @return DataProviderResponse
     */
    public function fetchLiveByCountryAllStatus(string $country): DataProviderResponse
    {
        $url = self::BASE_URL . "live/country/" . strtolower($country);
        $response = $this->dataProviderClient()->get($url);
        return $this->setPayloadFromResponse($response);
    }

    /**
     * GET Live By Country And Status After Date.
     * https://api.covid19api.com/live/country/south-africa/status/confirmed/date/2020-03-21T13:13:30Z
     *
     * @param string $country
     * @param Carbon $fromDate
     * @return DataProviderResponse
     */
    public function fetchLiveByCountryAndStatusAfterDate(string $country, Carbon $fromDate): DataProviderResponse
    {
        $url = self::BASE_URL . "live/country/" . strtolower($country) . "/status/confirmed/date/" . $fromDate->toIso8601String();
        $response = $this->dataProviderClient()->get($url);
        return $this->setPayloadFromResponse($response);
    }


    /**
     * Set Payload for Databox from response body.
     *
     * @param DataProviderResponse $response
     * @return DataProviderResponse
     */
    private function setPayloadFromResponse(DataProviderResponse $response): DataProviderResponse
    {
        if(!$response->ok())
        {
            return $response;
        }

        $response->setPayload($this->get_payload_all_status($response->getBody()));
        return $response;
    }

    /**
     * Parse response body to array of items.
     *
     * @param string $responseBody
     * @return array
     */
    private function get_items(string $responseBody): array
    {
        // json_decode is pretty week function
        // https://www.php.net/manual/en/function.json-decode.php
        // null is returned if the json cannot be decoded or if the encoded data is deeper than the nesting limit
        // also float values are truncated so this could be big issue for financial data
        return json_decode($responseBody);
    }

    /**
     * @param string $response
     * @return Payload[]
     */
    private function get_payload_all_status(string $response): array
    {
        $items = $this->get_items($response);

        return array_merge(...array_map(function ($metric) use ($items) {
            return $this->get_payload_for_metric($metric, $items);
        }, COVID19::getConstList()));
    }


    /**
     * Since we get more metric in single array,
     * we send each as different payload to Databox.
     *
     * Metrics: Confirmed, Deaths, Recovered, Active
     *
     * @param string $metric
     * @param array $items
     * @return array
     */
    private function get_payload_for_metric(string $metric, array $items): array
    {
        $attributeKeys = ["Country", "CountryCode", "Province", "City", "CityCode"];

        return array_map(function ($item) use ($metric, $attributeKeys) {

            $key = 'covid_' . strtolower($metric);

            $date = new Carbon($item->Date);

            $attributes = array_filter(
                (array) $item,
                function ($key) use ($attributeKeys) {
                    return in_array($key, $attributeKeys);
                },
                ARRAY_FILTER_USE_KEY
            );

            return new Payload($key, $item->{$metric}, $date, $attributes, "Cases");

        }, $items);
    }
}
