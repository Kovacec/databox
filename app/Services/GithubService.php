<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class GithubService
{
    const URL = "https://api.github.com/";

    private $token;

    /**
     * GithubService constructor.
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * @param string $owner
     * @param string $repo
     * @return array
     */
    public function listContributors(string $owner, string $repo): array
    {
        $url = self::URL . 'repos/' . $owner . '/' . $repo .'/contributors';

        $response = Http::withToken($this->token)
            ->withHeaders([
                "Accept" => "application/vnd.github.inertia-preview+json"
            ])
            ->get($url);

        $items = json_decode($response->body());

        if ($items === null)
        {
            return [];
        }

        if (!is_array($items))
        {
            return (array)$items;
        }

        return array_map(function ($item) {
            return $item->login;
        }, $items);
    }
}
