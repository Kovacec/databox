<?php

namespace App\Metrics;

class COVID19
{
    public const CONFIRMED = "Confirmed";

    public const DEATHS = "Deaths";

    public const RECOVERED = "Recovered";

    public const ACTIVE  = "Active";

    public static function getConstList(): array
    {
        return [
            self::CONFIRMED, self::DEATHS, self::RECOVERED, self:: ACTIVE
        ];
    }
}
