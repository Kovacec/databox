# Databox integration

To get things started quickly I chose lumen as a micro-framework.

## Installation


### PHP Code 

First you need to check the project from GitLab with git client: 

```bash
git clone git@gitlab.com:Kovacec/databox.git
```

### Dependencies

Change directory:

```bash
cd databox
```

Install dependencies with composer. 

```bash
composer install
```

Copy `.env.example` to `.env` and set up environment variables.

### Databox

To push data to Databox you will need a token.
[Get a token](https://developers.databox.com/api/#get-your-token) if you do not have one. 

Update `.env` file

```dotenv
DATABOX_TOKEN=your_token
```

### Database

To store push information and tokens we use a local database. 
For simplicity purposes you can use `sqlite`, but you could use any other database. 
For more information check: [databases](https://laravel.com/docs/8.x/database).
 
Create a new database.sqlite file:

```bash
touch /path_to_project/database/database.sqlite
```

And create a database schema with:

```bash
php artisan migrate
```

## Using the app

You can start with console commands. Just write in the root of the project
```bash 
php artisan
```
to get list of all available commands. 

Databox specific commands starts with prefix `databox`.

Examples:

```bash 
php artisan databox:push sales 200
php artisan databox:COVID slovenia
php artisan databox:COVID slovenia --new
```

## Periodic export 

Schedule is defined in `App\Console\Kernel`.

To enable periodic export of data as defined in `Kernel` you have to add this line to cron:

```text
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
```

### Set up Github API

In `.env` file set environment variables:

```dotenv
GITHUB_CLIENT_ID=client_id
GITHUB_CLIENT_SECRET=client_secret
```

To get an access token you have to first serve some front end. 

Use PHP's build in web server. Just run: 

```bash
php -S localhost:8082 -t ./public
```

in project root folder and visit `http://localhost:8082` in the browser.

### With Docker

```bash
git clone git@gitlab.com:Kovacec/databox.git
cd databox
```

Copy `.env.example` to `.env` and set up environment variables.

*INFO: Authorization callback URL in GitHub has to be set to `http://localhost:8082/github/callback`*  
[GitHub Settings](https://github.com/settings/developers)

```dotenv
DATABOX_TOKEN=token_from_databox
GITHUB_CLIENT_ID=client_id
GITHUB_CLIENT_SECRET=client_secret
```

Build an image from Dockerfile and run it.

```bash
sudo docker build -t databox_challange .
sudo docker run --name data -p 8082:8082 -d databox_challange
```

See available command: 

```bash
sudo docker exec -ti data php artisan
```

Push data to databox

```bash
sudo docker exec -ti data php artisan databox:github:contributors databox databox-php
```

Follow instructions.

## Used APIs

[COVID19 API](https://documenter.getpostman.com/view/10808728/SzS8rjbc#81447902-b68a-4e79-9df9-1b371905e9fa), seems to be unavailable a lot.  
[List of COVID19 APIs](https://covid-19-apis.postman.com/)
[Github API](https://docs.github.com/en/rest)

## License

Open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
