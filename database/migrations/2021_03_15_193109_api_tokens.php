<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ApiTokens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tokens', function(Blueprint $table) {

            $table->string("api")
                ->nullable(false)
                ->primary()
                ->comment("For which API is token.");

            $table->string("token")
                ->nullable(false)
                ->comment("Access Token");

            $table->string("type")
                ->nullable("false")
                ->comment("Token Type");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tokens');
    }
}
