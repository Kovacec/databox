<?php

use App\PushField;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataboxPushLog extends Migration
{
    const TABLE = 'pushes';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pushes', function(Blueprint $table) {
            $table->id()->autoIncrement();

            $table->dateTime(PushField::SENT_TIME)
                ->useCurrent()
                ->comment('Time of sending');

            $table->string(PushField::SERVICE)
                ->nullable(false)
                ->comment("Data provider.");

            $table->string(PushField::METRIC)
                ->nullable(false)
                ->comment('Metric sent to databox');

            $table->integer(PushField::COUNT)
                ->nullable(false)
                ->default(0)
                ->comment("Number of values/KPIs sent");

            $table->boolean(PushField::OK)
                ->default(false)
                ->comment('Sending successful or not');

            $table->string(PushField::ERROR)
                ->nullable(true)
                ->default(null)
                ->comment('Error message');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pushes');
    }
}
