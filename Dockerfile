FROM php:7.4-fpm-alpine

ENV \
  APP_DIR="/app" \
  APP_PORT=8082

RUN curl -sS https://getcomposer.org/installer | php -- \
  --install-dir=/usr/bin --filename=composer

COPY . /app

RUN cd "/app" && composer install && touch database/database.sqlite && php artisan migrate

WORKDIR /app

EXPOSE 8082

CMD php -S 0.0.0.0:8082 -t public
