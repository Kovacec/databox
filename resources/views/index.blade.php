<!doctype html>
<html lang="sl" class="full-heigh">
<head>
    <title class="">Databox Challenge</title>
    <meta name="description" content="Databox challenge.">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <style>
        html, body {height: 100%}
    </style>
</head>
<body class="flex flex-col">
    <div class="flex-none flex justify-center items-center p-5">
        <span>Click on service logo to get an access token for the service.</span>
    </div>
    @if(isset($error))
        <div class="flex-none bg-red-800 text-white flex justify-center items-center p-5">
            <span>{{ $error }}</span>
        </div>
    @endif
    <div class="flex flex-grow justify-center items-center">
        <a href="/github/redirect" class="opacity-80 hover:opacity-100 hover:cursor-pointer">
            <img src="/github-logo.jpg" class="w-64 cursor-pointer" alt="GitHub">
        </a>
    </div>
    <footer class="flex-none bg-grey-100">
        <div class="container mx-auto flex justify-between flex-row p-5">
            <div>by <a href="http://locate.tips/#/contact" class="underline">Bojan Kovačec</a></div>
        </div>
    </footer>
</body>
</html>
